package containertype;

import framework.FrameWorkBase;
import java.util.ArrayList;
import visualtype.BreakLineTag;

/**
 * A keretes tároló osztálya.
 *
 * @author Richard.Bogehold
 */
public class BorderedBlockContainer extends FrameWorkBase {

    private int borderWidth;
    private String borderStyle = "";
    private String borderColor = "";

    private ArrayList<FrameWorkBase> controlList = new ArrayList<>();

    /**
     * Visszaadja a borderWidth értékét.
     *
     * @return A borderWidth értéke.
     */
    public int getBorderWidth() {
        return borderWidth;
    }

    /**
     * Beállítja a borderWidth értékét.
     *
     * @param borderWidth A borderWidth értéke.
     */
    public void setBorderWidth(int borderWidth) {
        if (borderWidth < 1) {
            return;
        }

        this.borderWidth = borderWidth;
    }

    /**
     * Visszaadja a borderStyle értékét.
     *
     * @return A borderStyle értéke.
     */
    public String getBorderStyle() {
        return borderStyle;
    }

    /**
     * Beállítja a borderStyle értékét.
     *
     * @param borderStyle A borderStyle értéke.
     */
    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

    /**
     * Visszaadja a borderColor értékét.
     *
     * @return A borderColor értéke.
     */
    public String getBorderColor() {
        return borderColor;
    }

    /**
     * Beállítja a borderColor értékét.
     *
     * @param borderColor A borderColor értéke.
     */
    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * A BorderedBlockContainer konstruktora
     */
    public BorderedBlockContainer() {
        super("div");
    }

    /**
     * A controlList listához frameWorkBase típusú objektumot ad hozzá.
     *
     * @param frameWorkBase A hozzáadni kívánt objektum.
     * @return Önmagát adja vissza, hogy sorfolytonosan lehessen hívni.
     */
    public BorderedBlockContainer addControl(FrameWorkBase frameWorkBase) {

        controlList.add(frameWorkBase);
        return this;
    }

    /**
     * A controlList listához frameWorkBase típusú objektumot ad hozzá új
     * sorban.
     *
     * @param frameWorkBase A hozzáadni kívánt objektum.
     * @return Önmagát adja vissza, hogy sorfolytonosan lehessen hívni.
     */
    public BorderedBlockContainer addControlToNewLine(FrameWorkBase frameWorkBase) {

        controlList.add(new BreakLineTag());
        controlList.add(frameWorkBase);
        return this;
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(super.getTag());
        if (!"".equals(super.getId())) {
            builder.append(" id=\"").
                    append(super.getId()).
                    append("\"");
        }
        if (!"".equals(super.getName())) {
            builder.append(" name=\"").
                    append(super.getName()).
                    append("\"");
        }
        builder.append(" style=");

        builder.append("\"border-style: ").
                append(getBorderStyle()).
                append(";");

        builder.append(" border-color: ").
                append(getBorderColor()).
                append(";");

        builder.append(" border-width: ").
                append(getBorderWidth()).
                append("px;\">");

        if (!(controlList.isEmpty())) {
            controlList.stream().forEach((c) -> {
                builder.append(c.display());
            });
        }

        return builder.toString() + "</" + super.getTag() + ">";
    }
}
