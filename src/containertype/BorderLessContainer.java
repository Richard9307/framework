package containertype;

import framework.FrameWorkBase;
import java.util.ArrayList;
import visualtype.BreakLineTag;

/**
 * A keretnélküli tároló osztálya.
 *
 * @author Richard.Bogehold
 */
public class BorderLessContainer extends FrameWorkBase {

    private ArrayList<FrameWorkBase> controlList = new ArrayList<>();

    /**
     * A BorderLessContainer osztály konstruktora.
     */
    public BorderLessContainer() {
        super("div");
    }

    /**
     * A controlList listához frameWorkBase típusú objektumot ad hozzá.
     *
     * @param frameWorkBase A hozzáadni kívánt objektum.
     * @return Önmagát adja vissza, hogy sorfolytonosan lehessen hívni.
     */
    public BorderLessContainer addControl(FrameWorkBase frameWorkBase) {

        controlList.add(frameWorkBase);
        return this;
    }

    /**
     * A controlList listához frameWorkBase típusú objektumot ad hozzá új
     * sorban.
     *
     * @param frameWorkBase A hozzáadni kívánt objektum.
     * @return Önmagát adja vissza, hogy sorfolytonosan lehessen hívni.
     */
    public BorderLessContainer addControlToNewLine(FrameWorkBase frameWorkBase) {

        controlList.add(new BreakLineTag());
        controlList.add(frameWorkBase);
        return this;
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(super.getTag());
        if (!"".equals(super.getId())) {
            builder.append(" id=\"").
                    append(super.getId()).
                    append("\"");
        }
        if (!"".equals(super.getName())) {
            builder.append(" name=\"").
                    append(super.getName()).
                    append("\"");
        }
        builder.append(">");

        if (!(controlList.isEmpty())) {
            controlList.stream().forEach((c) -> {
                builder.append(c.display());
            });
        }
        return builder.toString() + "</" + super.getTag() + ">";
    }
}
