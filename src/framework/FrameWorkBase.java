package framework;

/**
 * Általános, absztrakt komponens, innen öröklődik a két altípus.
 *
 * @author Richard.Bogehold
 */
public abstract class FrameWorkBase {

    private String tag;
    private String innerHtml = "";
    private String name = "";
    private String id = "";
    private String value = "";

    /**
     * Vissza adja a tag típusát.
     *
     * @return A tag típusa.
     */
    public String getTag() {
        return tag;
    }

    /**
     * Visszaadja a tag belső szövegét.
     *
     * @return A tag belső szövege.
     */
    public String getInnerHtml() {
        return innerHtml;
    }

    /**
     * Beállítja a tag belső szövegét.
     *
     * @param innerHtml A tag belső szövege.
     */
    public void setInnerHtml(String innerHtml) {
        this.innerHtml = innerHtml;
    }

    /**
     * Visszaadja a name attribútum értékét.
     *
     * @return A name attribútum értéke.
     */
    public String getName() {
        return name;
    }

    /**
     * Beállítja a name attribútum értékét.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Visszaadja az id attribútum értékét.
     *
     * @return Az id attribútum értéke.
     */
    public String getId() {
        return id;
    }

    /**
     * Beállítja az id attribútum értékét.
     *
     * @param id Az id attribútum értéke.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Visszaadja a value attribútum értékét
     *
     * @return A value attribútum értéke.
     */
    public String getValue() {
        return value;
    }

    /**
     * Beállítja a value attribútum értékét.
     *
     * @param value A value attríbútum értéke.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * A FrameWorkBase osztály konstruktora.
     *
     * @param tag A Html tag típusa.
     */
    public FrameWorkBase(String tag) {
        this.tag = tag;
    }

    /**
     * A megjelenítő motor.
     *
     * @return Stringként adja vissza a felépített Html tag-et.
     */
    public String display() {
        StringBuilder builder = new StringBuilder();
      
        builder.append("<").
                append(tag);
        if (!"".equals(id)) {
            builder.append(" id=\"").
                    append(id).
                    append("\"");
        }
        if (!"".equals(name)) {
            builder.append(" name=\"").
                    append(name).
                    append("\"");
        }
        if (!"".equals(value)) {
            builder.append(" value=\"").
                    append(value).
                    append("\"");
        }
        return builder.toString() + ">" + innerHtml + "</" + tag + ">";
    }
}
