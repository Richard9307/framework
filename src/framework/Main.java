package framework;

import containertype.BorderLessContainer;
import containertype.BorderedBlockContainer;
import visualtype.BreakLineTag;
import visualtype.ButtonTag;
import visualtype.ComboBox;
import visualtype.LabelTag;
import visualtype.ParagraphTag;
import visualtype.RadioButtonTag;
import visualtype.TextBoxTag;

public class Main {

    public static void main(String[] args) {

        //Példányosítunk egy keretes blokkot.
        BorderedBlockContainer container = new BorderedBlockContainer();

        //Beállítjuk a keretes blokk szélességét, színét és stílusát.
        container.setBorderWidth(5);
        container.setBorderColor("black");
        container.setBorderStyle("solid");

        /*
         Példányosítunk két radiobuttont, létrehozáskor megadjuk a várt
         paramétereket.
         */
        RadioButtonTag maleRadioButton = new RadioButtonTag("male",
                "gender", "male");
        RadioButtonTag femaleRadioButton = new RadioButtonTag("female",
                "gender", "female");

        /*
         Példányosítjuk a labeleket, létrehozáskor megadjuk a várt 
         paramétereket.
         */
        LabelTag maleLabel = new LabelTag(maleRadioButton, "Male",
                "radio", "gender", "male", "male");
        LabelTag femaleLabel = new LabelTag(femaleRadioButton, "Female",
                "radio", "gender", "female", "female");

        /*
         Létrehozunk egy gombot és a setText metódussal megadjuk 
         a gombon megjelenő szöveget.
         */
        ButtonTag chooseButton = new ButtonTag();
        chooseButton.setText("Choose");

        /*
         Példányosítunk egy újabb gombot, létrehozáskor megadjuk a várt
         paramétert.
         */
        ButtonTag skipButton = new ButtonTag("Skip");

        /*Létrehozunk egy content paragrafust, és a setContent metódusával
         megadjuk a szöveget.
         */
        ParagraphTag content = new ParagraphTag();
        content.setContent("Gender:");

        //Hozzáadjuk a keretes blokkhoz az elemeket.        
        container.addControl(content);
        container.addControl(maleLabel);
        container.addControl(maleRadioButton);
        container.addControlToNewLine(femaleLabel);
        container.addControl(femaleRadioButton);
        //Létrehozunk egy sortörést.
        container.addControl(new BreakLineTag());
        container.addControlToNewLine(chooseButton);
        container.addControl(skipButton);
        //Létrehozunk egy sortörést.
        container.addControl(new BreakLineTag());

        //Példányosítunk egy keret nélküli blokkot.
        BorderLessContainer borderlessContainer = new BorderLessContainer();

        /*
         Példányosítunk két paragrafust, majd a setContent metódussal megadjuk
         a szövegeket.
         */
        ParagraphTag eMailField = new ParagraphTag();
        ParagraphTag passwordField = new ParagraphTag();
        ParagraphTag serverField = new ParagraphTag();
        eMailField.setContent("E-mail address:");
        passwordField.setContent("Password:");
        serverField.setContent("Server:");

        //Létrehozzuk a ComboBoxot és hozzáadjuk az opciókat.
        ComboBox serverComboBox = new ComboBox();
        serverComboBox.addOption("test", "Test server");
        serverComboBox.addOption("development", "Development server");
        serverComboBox.addOption("production", "Production server");

        /*
         Hozzáadjuk az elemeket a keret nélküli blokkhoz, közben létrehozunk 
         két TextBoxTaget, megadjuk a várt paramétereiket, 
         azokat is hozzáadjuk a blokkhoz.
         */
        borderlessContainer.addControl(eMailField);
        borderlessContainer.addControl(new TextBoxTag("email", "your e-mail "
                + "address..."));
        borderlessContainer.addControlToNewLine(passwordField);
        borderlessContainer.addControl(new TextBoxTag("password", "****"));
        borderlessContainer.addControlToNewLine(passwordField);
        borderlessContainer.addControl(serverComboBox);

        //A keretes blokk kiíratása konzolra.        
        System.out.println(container.display());
        //A keret nélküli blokk kiíratása konzolra.
        System.out.println(borderlessContainer.display());
        //Egy gomb elem kiíratása konzolra.
        System.out.println(skipButton.display());

    }

}
