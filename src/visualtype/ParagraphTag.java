package visualtype;

import framework.FrameWorkBase;

/**
 * A paragraph HTML tag osztálya
 *
 * @author Richard.Bogehold
 */
public class ParagraphTag extends FrameWorkBase {

    private String content;

    /**
     * A ParagraphTag konstruktora.
     */
    public ParagraphTag() {
        super("p");
    }

    /**
     * Visszaadja a megadott szöveget.
     * @return A megadott szöveg.
     */
    public String getContent() {
        return content;
    }

    /***
     * Beállítja a szöveget.
     * @param content A megjelenő szöveg.
     */
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(getTag()).
                append(">").
                append(getContent()).
                append("</p>");
        
        return builder.toString();

    }

}
