package visualtype;

import framework.FrameWorkBase;

/**
 * A szöveges beviteli mező Html tag osztálya.
 *
 * @author Richard.Bogehold
 */
public class TextBoxTag extends FrameWorkBase {

    /**
     * A szöveges beviteli mező paraméter nélküli konstruktora.
     */
    public TextBoxTag() {
        super("input");
    }

    /**
     * *
     * A szöveges beviteli mező konstruktora
     *
     * @param name A name attribútum értéke.
     * @param value A value attribútum értéke.
     */
    public TextBoxTag(String name, String value) {
        super("input");
        super.setName(name);
        super.setValue(value);
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(getTag()).
                append(" type=\"text\"");

        if (!"".equals(getName())) {
            builder.append(" name=\"").
                    append(getName()).
                    append("\"");
        }
        if (!"".equals(getValue())) {
            builder.append(" value=\"").
                    append(getValue()).
                    append("\"");
        }
        return builder.toString() + ">";
    }
}
