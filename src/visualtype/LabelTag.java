package visualtype;

import framework.FrameWorkBase;

/**
 * A label Html tag osztálya
 *
 * @author Richard.Bogehold
 */
public class LabelTag extends FrameWorkBase {

    private String type = "";
    private FrameWorkBase forAttribute = null;

    /**
     * A LabelTag osztály paraméter nélküli konstruktora.
     */
    public LabelTag() {
        super("label");
    }

    /**
     * A LabelTag osztály konstruktora.
     *
     * @param forAttribute A forAttribute attribútum értéke.
     * @param innerHtml A LabelTag felirata.
     * @param type A type attribútum értéke.
     * @param name A name attribútum értéke.
     * @param id Az id attribútum értéke.
     * @param value A value attribútum értéke.
     */
    public LabelTag(FrameWorkBase forAttribute, String innerHtml, String type,
            String name, String id, String value) {
        super("label");
        this.setForAttribute(forAttribute);
        super.setInnerHtml(innerHtml);
        this.setType(type);
        super.setName(name);
        super.setId(id);
        super.setValue(value);
    }

    /**
     * Visszaadja a type attríbútum értékét.
     *
     * @return A type attribútum értéke.
     */
    public String getType() {
        return type;
    }

    /**
     * Beállítja a type attribútum értékét.
     *
     * @param type A type attribútum értéke.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Visszaadja a forAttribute attribútum értékét.
     *
     * @return Az forAttribute attribútum értéke.
     */
    public FrameWorkBase getForAttribute() {
        return forAttribute;
    }

    /**
     * Beállítja a forAttribute attribútum értékét.
     *
     * @param f A forAttribute értéke.
     */
    public void setForAttribute(FrameWorkBase f) {
        this.forAttribute = f;
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(getTag()).
                append(" for=\"").
                append(getForAttribute() == null ? ""
                                : getForAttribute().getId()).
                append("\">").
                append(getInnerHtml()).
                append("</").
                append(getTag()).
                append(">");

        return builder.toString();
    }
}
