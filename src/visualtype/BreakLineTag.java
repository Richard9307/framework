package visualtype;

import framework.FrameWorkBase;

/**
 * A sortörés Html tag osztálya.
 *
 * @author Richard.Bogehold
 */
public class BreakLineTag extends FrameWorkBase {

    /**
     * A BreakLineTag osztály konstruktora.
     */
    public BreakLineTag() {
        super("br");
    }

    @Override
    public String display() {
        return "<br>";
    }
}
