package visualtype;

import framework.FrameWorkBase;
import java.util.HashMap;
import java.util.Map;

/**
 * A legördülő lista Html tag osztálya
 *
 * @author Richard.Bogehold
 */
public class ComboBox extends FrameWorkBase {

    private HashMap<String, String> comboList = new HashMap<>();

    /**
     * A legördülő menü osztály konstruktora.
     */
    public ComboBox() {
        super("select");
    }

    /**
     * A comboList HashMaphez hozzáad egy key-t, illetve value-t.
     *
     * @param value A ComboBox-ban szereplő opció value attribútumja.
     * @param display A ComboBox-ban szereplő opció szövege.
     * @return Önmagát adja vissza, hogy újabb elemeket lehessen hozzáadni a
     * HashMaphez.
     */
    public ComboBox addOption(String value, String display) {

        comboList.put(value, display);

        return this;
    }

    /**
     * Elvesz egy opciót a ComboBox-ból.
     *
     * @param value A ComboBoxban lévő value attribútum.
     * @return Önmagát adja vissza, hogy újabb elemeket lehessen elvenni a
     * HashMapből.
     */
    public ComboBox removeOption(String value) {

        comboList.remove(value);

        return this;
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(getTag());
        if (!"".equals(getId())) {
            builder.append(" id=\"").
                    append(getId()).
                    append("\"");
        }
        if (!"".equals(getName())) {
            builder.append(" name=\"").
                    append(getName()).
                    append("\"");
        }

        builder.append(">");

        for (Map.Entry<String, String> entrySet : comboList.entrySet()) {
            builder.append("<option ").
                    append("value=\"").
                    append(entrySet.getKey()).
                    append("\">").
                    append(entrySet.getValue()).
                    append("</option>");
        }

        return builder.toString() + "</" + getTag() + ">";
    }
}
