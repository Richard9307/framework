    package visualtype;

import framework.FrameWorkBase;

/**
 * A gomb HTML tag osztálya
 *
 * @author Richard.Bogehold
 */
public class ButtonTag extends FrameWorkBase {

    /**
     * A ButtonTag osztály paraméter nélküli konstruktora.
     */
    public ButtonTag() {
        super("button");
    }

    /**
     * A ButtonTag osztály konstruktora.
     *
     * @param innerHtml A ButtonTag felirata.
     */
    public ButtonTag(String innerHtml) {
        super("button");
        super.setInnerHtml(innerHtml);
    }

    /**
     * Beállítja a ButtonTag feliratát
     *
     * @param text A felirat.
     */
    public void setText(String text) {
        setInnerHtml(text);
    }
}
