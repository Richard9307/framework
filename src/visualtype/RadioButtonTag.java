package visualtype;

import framework.FrameWorkBase;

/**
 * A radiobutton Html tag osztálya.
 *
 * @author Richard.Bogehold
 */
public class RadioButtonTag extends FrameWorkBase {

    /**
     * A radiobutton paraméter nélküli konstruktora.
     */
    public RadioButtonTag() {
        super("input");
    }

    /**
     * A radiobutton konstruktora
     *
     * @param id Az id attribútum értéke.
     * @param name A name attribútum értéke.
     * @param value A value attribútum értéke.
     */
    public RadioButtonTag(String id, String name, String value) {
        super("input");
        super.setId(id);
        super.setName(name);
        super.setValue(value);
    }

    @Override
    public String display() {
        StringBuilder builder = new StringBuilder();

        builder.append("<").
                append(getTag()).
                append(" type=\"radio\"");

        if (!"".equals(getId())) {
            builder.append(" name=\"").
                    append(getId()).
                    append("\"");
        }

        if (!"".equals(getName())) {
            builder.append(" name=\"").
                    append(getName()).
                    append("\"");
        }
        if (!"".equals(getValue())) {
            builder.append(" value=\"").
                    append(getValue()).
                    append("\"");
        }
        return builder.toString() + ">";
    }
}
